package ru.gotoss.castle.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import ru.gotoss.castle.assets.UiTextureAssets;

/**
 * Created by gotos on 7/5/2016.
 */
public class UiHelper {
    public static ButtonStyle defaultButtonStyle;
    public static TextButton.TextButtonStyle defaultTextButtonStyle;
    public static Window.WindowStyle defaulWindowStyle;
    public static Label.LabelStyle defaultLabelStyle;

    public static int defaultButtonWidth = 125;
    public static int defaultButtonHeight = 32;

    public static void init(BitmapFont font) {
        defaultButtonStyle = new ButtonStyle();
        defaultButtonStyle.up = new TextureRegionDrawable(new TextureRegion(UiTextureAssets.btnBackground));
        defaultButtonStyle.over = new TextureRegionDrawable(new TextureRegion(UiTextureAssets.btnHover));
        defaultButtonStyle.down = new TextureRegionDrawable(new TextureRegion(UiTextureAssets.btnPressed));

        defaultTextButtonStyle = new TextButton.TextButtonStyle();
        defaultTextButtonStyle.up = new TextureRegionDrawable(new TextureRegion(UiTextureAssets.btnBackground));
        defaultTextButtonStyle.over = new TextureRegionDrawable(new TextureRegion(UiTextureAssets.btnHover));
        defaultTextButtonStyle.down = new TextureRegionDrawable(new TextureRegion(UiTextureAssets.btnPressed));
        defaultTextButtonStyle.font = font;

        defaulWindowStyle = new Window.WindowStyle(font, font.getColor(), new TextureRegionDrawable(new TextureRegion(UiTextureAssets.btnBackground)));

        defaultLabelStyle = new Label.LabelStyle(font, font.getColor());
    }

    public static int centerX(int width) {
        return (Gdx.graphics.getWidth() / 2) - (width / 2);
    }

    public static int centerY(int height) {
        return (Gdx.graphics.getHeight() / 2) - (height / 2);
    }

    public static Button getDeafultButton() {
        return new Button(defaultButtonStyle);
    }

    public static Button getDeafultTextButton() {
        return new TextButton("Button", defaultTextButtonStyle);
    }
}
