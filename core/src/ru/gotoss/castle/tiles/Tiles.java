package ru.gotoss.castle.tiles;

import com.badlogic.gdx.graphics.g2d.Sprite;
import ru.gotoss.castle.assets.SpriteTextureAssets;

/**
 * Created by gotos on 7/5/2016.
 */
public class Tiles {
    public static FloorTile floor1 = new FloorTile(new Sprite(SpriteTextureAssets.floor1Texture));
    public static FloorTile floor2 = new FloorTile(new Sprite(SpriteTextureAssets.floor2Texture));
    public static WallTile wall = new WallTile(new Sprite(SpriteTextureAssets.wallTexture));
    public static GoldVineTile goldVine = new GoldVineTile(new Sprite(SpriteTextureAssets.goldVineTexture));
    public static DamageTile damage1 = new DamageTile(new Sprite(SpriteTextureAssets.damage1Texture));
    public static DamageTile damage2 = new DamageTile(new Sprite(SpriteTextureAssets.damage2Texture));
    public static DamageTile damage3 = new DamageTile(new Sprite(SpriteTextureAssets.damage3Texture));
}
