package ru.gotoss.castle.tiles;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by gotos on 7/5/2016.
 */
public class GoldVineTile extends WallTile {
    public GoldVineTile(Sprite sprite) {
        super(sprite);
        setSolid(true);
        setName("gold vine");
        setMaxHealth(3);
        setHealth(3);
    }
}
