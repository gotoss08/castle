package ru.gotoss.castle.tiles;

import com.badlogic.gdx.graphics.g2d.Sprite;
import ru.gotoss.castle.core.Tile;

/**
 * Created by gotos on 7/5/2016.
 */
public class DamageTile extends Tile {
    public DamageTile(Sprite sprite) {
        setName("");
        addSprite(sprite);
        setSolid(false);
    }
}
