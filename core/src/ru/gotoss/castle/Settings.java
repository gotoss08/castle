package ru.gotoss.castle;

/**
 * Created by gotos on 7/5/2016.
 */

public class Settings {
    public static final String NAME = "Castle";
    public static final String VERSION = "v0.0.1";
    public static final String TITLE = String.format("%s - %s", NAME, VERSION);
    public static final int WIDTH = 640;
    public static final int HEIGHT = 480;
    public static final int TILE_SIZE = 32;
}
