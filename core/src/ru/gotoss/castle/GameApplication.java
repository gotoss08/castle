package ru.gotoss.castle;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import ru.gotoss.castle.assets.SpriteTextureAssets;
import ru.gotoss.castle.assets.UiTextureAssets;
import ru.gotoss.castle.screens.MainMenuScreen;
import ru.gotoss.castle.ui.UiHelper;

public class GameApplication extends Game {

    public SpriteBatch batch;
    public BitmapFont font;

    UiTextureAssets uiTextureAssets;
    SpriteTextureAssets spriteTextureAssets;

    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();

        uiTextureAssets = new UiTextureAssets();
        uiTextureAssets.load();

        spriteTextureAssets = new SpriteTextureAssets();
        spriteTextureAssets.load();

        UiHelper.init(font);
        this.setScreen(new MainMenuScreen(this));
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        super.dispose();
    }
}
