package ru.gotoss.castle.graphics;

import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * Created by gotos on 7/6/2016.
 */
public class Camera {

    private int cellSize;

    private int worldWidth;
    private int worldHeight;

    private int cameraWorldWidth;
    private int cameraWorldHeight;

    private int cameraX;
    private int cameraY;

    private int centerCameraX;
    private int centerCameraY;

    private int cameraWidth;
    private int cameraHeight;

    private int renderX;
    private int renderY;
    private int renderWidth;
    private int renderHeight;

    private OrthographicCamera camera;

    public Camera(int cellSize, int worldWidth, int worldHeight) {

        this.setCellSize(cellSize);

        this.setWorldWidth(worldWidth);
        this.setWorldHeight(worldHeight);
        create(worldWidth, worldHeight);
    }

    public Camera(int cellSize, int worldWidth, int worldHeight, int cameraWidth, int cameraHeight) {

        this.setCellSize(cellSize);

        this.setWorldWidth(worldWidth);
        this.setWorldHeight(worldHeight);
        create(cameraWidth, cameraHeight);
    }

    public void create(int cameraWidth, int cameraHeight){
        setCameraWorldWidth(worldWidth * cellSize);
        setCameraWorldHeight(worldHeight * cellSize);

        setCameraWidth((cameraWidth / 8) * cellSize);
        setCameraHeight((cameraHeight / 8) * cellSize);

        setCenterCameraX(getCameraWidth() / 2);
        setCenterCameraY(getCameraHeight() / 2);

        setCameraX(getCenterCameraX());
        setCameraY(getCenterCameraY());

        setCamera(new OrthographicCamera(getCameraWidth(), getCameraHeight()));
        getCamera().position.set(getCameraX(), getCameraY(), 0);
        getCamera().update();
    }

    public void update() {
        getCamera().position.set(getCameraX(), getCameraY(), 0);
        getCamera().update();


        setRenderX((getCameraX() - (getCameraWidth() / 2)) / cellSize);
        setRenderY((getCameraY() - (getCameraHeight() / 2)) / cellSize);
        setRenderWidth((getRenderX() + (getCameraWidth() / cellSize)));
        setRenderHeight((getRenderY() + (getCameraHeight() / cellSize)));
    }

    public void translate(int x, int y) {
        int mx = getCameraX() + x;
        int my = getCameraY() + y;

        if (mx >= getCameraWidth() / 2 && mx <= getCameraWorldHeight() - getCameraWidth() / 2 && my >= getCameraHeight() / 2 && my <= getCameraWorldHeight() - getCameraHeight() / 2) {
            setCameraX(mx);
            setCameraY(my);
        }
    }

    public void setPosition(int x, int y) {
        int mx = x;
        int my = y;

        if (mx < getCameraWidth() / 2) {
            mx = getCameraWidth() / 2;
        }
        if (mx > getCameraWorldWidth() - getCameraWidth() / 2) {
            mx = getCameraWorldWidth() - getCameraWidth() / 2;
        }
        if (my < getCameraHeight() / 2) {
            my = getCameraHeight() / 2;
        }
        if (my > getCameraWorldHeight() - getCameraHeight() / 2) {
            my = getCameraWorldHeight() - getCameraHeight() / 2;
        }

        setCameraX(mx);
        setCameraY(my);
    }


    public void resize() {

    }

    public int getCellSize() {
        return cellSize;
    }

    public void setCellSize(int cellSize) {
        this.cellSize = cellSize;
    }

    public int getWorldWidth() {
        return worldWidth;
    }

    public void setWorldWidth(int worldWidth) {
        this.worldWidth = worldWidth;
    }

    public int getWorldHeight() {
        return worldHeight;
    }

    public void setWorldHeight(int worldHeight) {
        this.worldHeight = worldHeight;
    }

    public int getCameraWorldWidth() {
        return cameraWorldWidth;
    }

    public void setCameraWorldWidth(int cameraWorldWidth) {
        this.cameraWorldWidth = cameraWorldWidth;
    }

    public int getCameraWorldHeight() {
        return cameraWorldHeight;
    }

    public void setCameraWorldHeight(int cameraWorldHeight) {
        this.cameraWorldHeight = cameraWorldHeight;
    }

    public int getCameraX() {
        return cameraX;
    }

    public void setCameraX(int cameraX) {
        this.cameraX = cameraX;
    }

    public int getCameraY() {
        return cameraY;
    }

    public void setCameraY(int cameraY) {
        this.cameraY = cameraY;
    }

    public int getCenterCameraX() {
        return centerCameraX;
    }

    public void setCenterCameraX(int centerCameraX) {
        this.centerCameraX = centerCameraX;
    }

    public int getCenterCameraY() {
        return centerCameraY;
    }

    public void setCenterCameraY(int centerCameraY) {
        this.centerCameraY = centerCameraY;
    }

    public int getCameraWidth() {
        return cameraWidth;
    }

    public void setCameraWidth(int cameraWidth) {
        this.cameraWidth = cameraWidth;
    }

    public int getCameraHeight() {
        return cameraHeight;
    }

    public void setCameraHeight(int cameraHeight) {
        this.cameraHeight = cameraHeight;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public void setCamera(OrthographicCamera camera) {
        this.camera = camera;
    }

    public int getRenderX() {
        return renderX;
    }

    public void setRenderX(int renderX) {
        this.renderX = renderX;
    }

    public int getRenderY() {
        return renderY;
    }

    public void setRenderY(int renderY) {
        this.renderY = renderY;
    }

    public int getRenderWidth() {
        return renderWidth;
    }

    public void setRenderWidth(int renderWidth) {
        this.renderWidth = renderWidth;
    }

    public int getRenderHeight() {
        return renderHeight;
    }

    public void setRenderHeight(int renderHeight) {
        this.renderHeight = renderHeight;
    }
}