package ru.gotoss.castle.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gotos on 7/5/2016.
 */
public abstract class TextureAsset {

    private List<Texture> textures = new ArrayList<Texture>();
    private String assetFolder;

    public abstract void load();

    /**
     * asset folder:
     *
     * @param assetFolder use getAssetFolder to change
     * @return
     */
    protected Texture loadTexture(String path) {
        Texture t = new Texture(Gdx.files.internal(assetFolder + "/" + path));
        textures.add(t);
        return t;
    }

    public void dispose() {
        for (Texture t : textures) {
            t.dispose();
        }
    }

    protected void setAssetFolder(String assetFolder) {
        this.assetFolder = assetFolder;
    }
}
