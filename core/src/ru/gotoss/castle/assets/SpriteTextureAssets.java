package ru.gotoss.castle.assets;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by gotos on 7/5/2016.
 */
public class SpriteTextureAssets extends TextureAsset {

    public static Sprite floor1Texture;
    public static Sprite floor2Texture;
    public static Sprite wallTexture;
    public static Sprite goldVineTexture;
    public static Sprite damage1Texture;
    public static Sprite damage2Texture;
    public static Sprite damage3Texture;
    public static Sprite bounds;

    public static Sprite player;

    @Override
    public void load() {
        setAssetFolder("sprites");
        floor1Texture = new Sprite(loadTexture("floor_01.png"));
        floor2Texture = new Sprite(loadTexture("floor_02.png"));
        wallTexture = new Sprite(loadTexture("wall_01.png"));
        goldVineTexture = new Sprite(loadTexture("gold_vine.png"));
        damage1Texture = new Sprite(loadTexture("block_damage_01.png"));
        damage2Texture = new Sprite(loadTexture("block_damage_02.png"));
        damage3Texture = new Sprite(loadTexture("block_damage_03.png"));
        bounds = new Sprite(loadTexture("bounds.png"));
        player = new Sprite(loadTexture("player.png"));
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
