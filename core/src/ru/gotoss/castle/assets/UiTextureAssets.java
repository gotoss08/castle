package ru.gotoss.castle.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gotos on 7/5/2016.
 */
public class UiTextureAssets extends TextureAsset {

    public static Pixmap cursor;

    // Ui
    public static Texture selection;
    public static Texture selectionPressed;
    // Button
    public static Texture btnBackground;
    public static Texture btnHover;
    public static Texture btnPressed;

    // Text field
    public static Texture textCursor;

    // Window
    public static Texture winBackground;

    private static List<Texture> textures;

    @Override
    public void load() {
        setAssetFolder("textures");
        textures = new ArrayList<Texture>();

        cursor = new Pixmap(Gdx.files.internal("textures/cursor.png"));

        selection = loadTexture("selection.png");
        selectionPressed = loadTexture("selection_pressed.png");
        textCursor = loadTexture("text_cursor.png");
        btnBackground = loadTexture("btn_background.png");
        btnHover = loadTexture("btn_hover.png");
        btnPressed = loadTexture("btn_pressed.png");
        winBackground = loadTexture("win_background.png");
    }

    @Override
    public void dispose() {
        super.dispose();
        cursor.dispose();
    }
}
