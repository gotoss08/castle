package ru.gotoss.castle.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import javafx.scene.paint.Color;
import ru.gotoss.castle.GameApplication;
import ru.gotoss.castle.ui.UiHelper;

/**
 * Created by gotos on 7/5/2016.
 */
public class MainMenuScreen implements Screen {
    final GameApplication game;

    Music mainMenuTheme;

    private Stage stage;

    private TextButton playButton;
    private TextButton exitButton;

    public MainMenuScreen(final GameApplication game) {
        this.game = game;

        mainMenuTheme = Gdx.audio.newMusic(Gdx.files.internal("audio/mainMenuTheme.mp3"));
        mainMenuTheme.setVolume(0.5f);
        mainMenuTheme.setLooping(true);

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        Gdx.app.log("Test", "test");

        playButton = new TextButton("Play", UiHelper.defaultTextButtonStyle);
        playButton.setBounds(UiHelper.centerX(125), UiHelper.centerY(-(32 + 8)), UiHelper.defaultButtonWidth, UiHelper.defaultButtonHeight);
        playButton.addAction(new Action() {
            @Override
            public boolean act(float delta) {
                if (playButton.isPressed()) {
                    game.setScreen(new GameScreen(game));
                    dispose();
                    return true;
                } else {
                    return false;
                }
            }
        });

        exitButton = new TextButton("Quit", UiHelper.defaultTextButtonStyle);
        exitButton.setBounds(UiHelper.centerX(125), UiHelper.centerY(32), UiHelper.defaultButtonWidth, UiHelper.defaultButtonHeight);
        exitButton.addAction(new Action() {
            @Override
            public boolean act(float delta) {
                if (exitButton.isPressed()) {
                    Gdx.app.exit();
                    return true;
                } else {
                    return false;
                }
            }
        });


        stage.addActor(playButton);
        stage.addActor(exitButton);
    }

    @Override
    public void show() {
        mainMenuTheme.play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor((float) Color.CORNFLOWERBLUE.getRed(), (float) Color.CORNFLOWERBLUE.getGreen(), (float) Color.CORNFLOWERBLUE.getBlue(), 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        mainMenuTheme.dispose();
        stage.dispose();
    }
}
