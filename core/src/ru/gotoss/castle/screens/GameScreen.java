package ru.gotoss.castle.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import ru.gotoss.castle.GameApplication;
import ru.gotoss.castle.assets.SpriteTextureAssets;
import ru.gotoss.castle.assets.UiTextureAssets;
import ru.gotoss.castle.core.Creature;
import ru.gotoss.castle.core.Tile;
import ru.gotoss.castle.core.World;
import ru.gotoss.castle.graphics.Camera;
import ru.gotoss.castle.tiles.GoldVineTile;
import ru.gotoss.castle.tiles.Tiles;
import ru.gotoss.castle.tiles.WallTile;
import ru.gotoss.castle.ui.UiHelper;

import java.util.ArrayList;
import java.util.List;

import static ru.gotoss.castle.tiles.Tiles.damage1;

/**
 * Created by gotos on 7/5/2016.
 */
public class GameScreen implements Screen {

    final GameApplication game;
    Music mainTheme;
    Vector3 camRayOrigin;
    // UI
    TextButton inventoryButton;
    private Camera cam;
    private SpriteBatch batch;
    private BitmapFont font;
    private SpriteBatch guiBatch;
    private Creature player;
    private World world;
    private Stage stage;
    private int selX = 0;
    private int selY = 0;
    private boolean drawTileCursor;
    private float mouseX;
    private float mouseY;
    private boolean debug;
    private int cellSize;
    private List<Rectangle> rectangles;

    public GameScreen(final GameApplication game) {
        this.game = game;

        System.out.println("configuring sounds...");
        mainTheme = Gdx.audio.newMusic(Gdx.files.internal("audio/mainTheme.mp3"));
        System.out.println("configuring sound volume");
        mainTheme.setVolume(0.5f);
        System.out.println("configuring sound looping");
        mainTheme.setLooping(true);

        System.out.println("configuring cursor");
        // Set custom cursor
        Gdx.graphics.setCursor(Gdx.graphics.newCursor(UiTextureAssets.cursor, 4, 0));

        System.out.println("configuring sprite batch");
        // SpriteBatch and BitmapFont setup
        batch = game.batch;
        System.out.println("configuring gui sprite batch");
        guiBatch = new SpriteBatch();
        System.out.println("configuring fonts");
        font = game.font;

        // Stage and UI setup
        System.out.println("creting stage");
        stage = new Stage();
        System.out.println("configuring input processor");
        Gdx.input.setInputProcessor(stage);

        rectangles = new ArrayList<Rectangle>();

        System.out.println("creating stage`s ui");
        createUI();

        cellSize = 32;

        // World setup
        System.out.println("creting world");
        world = new World(500, 500);
        System.out.println("generating world");
        world.generate();

        System.out.println("creting player");
        player = new Creature("player", new Sprite(SpriteTextureAssets.player), new Vector2(0, 0));

        System.out.println("placing player at random location");
        world.putCreatureAtRandomPlace(player);


        // Constructs a new OrthographicCamera, using the given viewport width and height
        // Height is multiplied by aspect ratio.
        System.out.println("creting new orthographic camera");
        cam = new Camera(cellSize, world.getWidth(), world.getHeight(), 100, 100);
        System.out.println(String.format("x%s y%s", (int) (player.getPosition().x), (int) (player.getPosition().y)));
        cam.setPosition((int) (player.getPosition().x * cellSize), (int) (player.getPosition().y * cellSize));
        System.out.println("updating camera");
        cam.update();
    }

    private void createUI() {
        System.out.println("    creating inventory button");
        inventoryButton = new TextButton("Inventory", UiHelper.defaultTextButtonStyle);
        System.out.println("    processing ui element");
        processUiElement(inventoryButton, UiHelper.centerX(UiHelper.defaultButtonWidth), 10, UiHelper.defaultButtonWidth, UiHelper.defaultButtonHeight);
    }


    private void processUiElement(Table table, int x, int y, int width, int height) {
        System.out.println("        setting bounds");
        table.setBounds(x, y, width, height);
        System.out.println("        adding to rectangles array");
        rectangles.add(new Rectangle(x, y, width, height));
        System.out.println("        adding actor to stage");
        stage.addActor(table);
    }


    @Override
    public void show() {
        mainTheme.play();
    }

    @Override
    public void render(float delta) {
        handleInput();
        cam.setPosition((int) (player.getPosition().x * cellSize), (int) (player.getPosition().y * cellSize));
        cam.update();
        camRayOrigin = cam.getCamera().getPickRay(Gdx.input.getX(), Gdx.input.getY()).origin;

        selX = (int) camRayOrigin.x / 32;
        selY = (int) camRayOrigin.y / 32;

        batch.setProjectionMatrix(cam.getCamera().combined);

        Gdx.gl.glClearColor(0, 0.2f, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        drawTileCursor = true;

        for (Rectangle r : rectangles) {
            if (r.contains(mouseX, mouseY)) {
                drawTileCursor = !r.contains(mouseX, mouseY);
            }
        }

        // Main batch render
        batch.begin();

        world.render(batch, cam.getRenderX(), cam.getRenderY(), cam.getRenderWidth(), cam.getRenderHeight());

        // Draw grid selection

        if (drawTileCursor) {
            if (Gdx.input.isButtonPressed(0))
                batch.draw(UiTextureAssets.selectionPressed, selX * 32, selY * 32, 32, 32);
            else
                batch.draw(UiTextureAssets.selection, selX * 32, selY * 32, 32, 32);
        }


        batch.end();

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        // Debug render
        guiBatch.begin();

        if (debug) {
            font.draw(guiBatch, "fps: " + Gdx.graphics.getFramesPerSecond(), 10, Gdx.graphics.getHeight() - font.getLineHeight() * 1);
            font.draw(guiBatch, "mouse x=" + Gdx.input.getX() + " y=" + Gdx.input.getY(), 10, Gdx.graphics.getHeight() - font.getLineHeight() * 2);
            font.draw(guiBatch, "mouse normalized x=" + mouseX + " y=" + mouseY, 10, Gdx.graphics.getHeight() - font.getLineHeight() * 3);
            if (selX > 0 && selX < Gdx.graphics.getWidth() && selY > 0 && selY < Gdx.graphics.getHeight())
                font.draw(guiBatch, String.format("%s[%s][%s] %s", world.getTile(selX, selY).getName(), selX, selY, world.creature(selX, selY) == null ? "" : world.creature(selX, selY).getName()), 10, font.getLineHeight() * 1);
            font.draw(guiBatch, "player x=" + player.getPosition().x + " y=" + player.getPosition().y, 10, Gdx.graphics.getHeight() - font.getLineHeight() * 4);
        }
        guiBatch.end();
    }

    private void handleInput() {
        mouseX = Gdx.input.getX();
        mouseY = Gdx.graphics.getHeight() - Gdx.input.getY();

        int speed = 1;
        // Arrows
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            movePlayer(0, -speed);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            movePlayer(0, speed);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
            movePlayer(-speed, 0);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            movePlayer(speed, 0);
        }

        // Numpad
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_1)) {
            movePlayer(-speed, -speed);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_2)) {
            movePlayer(0, -speed);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_3)) {
            movePlayer(speed, -speed);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_4)) {
            movePlayer(-speed, 0);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_5)) {
            movePlayer(0, 0);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_6)) {
            movePlayer(speed, 0);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_7)) {
            movePlayer(-speed, speed);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_8)) {
            movePlayer(0, speed);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_9)) {
            movePlayer(speed, speed);
        }


        if (Gdx.input.isKeyJustPressed(Input.Keys.I)) {
            System.out.println("I justtyped");
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            dispose();
            game.setScreen(new GameScreen(game));
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            dispose();
            game.setScreen(new MainMenuScreen(game));
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.F1)) {
            debug = !debug;
        }
    }

    private void movePlayer(int x, int y) {

        int mx = (int) player.getPosition().x + x;
        int my = (int) player.getPosition().y + y;

        if (world.checkBorder(mx, my)) {
            if (world.getTile(mx, my).isSolid()) {
                System.out.println(String.format("world[%s][%s].name = %s is solid! You can not walk through this.", mx, my, world.getTile(mx, my).getName()));
                if (world.getTile(mx, my) instanceof WallTile) {
                    if (world.getTile(mx, my) instanceof GoldVineTile) {
                        world.getTile(mx, my).setHealth(world.getTile(mx, my).getHealth() - 1);
                        if (world.getTile(mx, my).getHealth() <= 0) {
                            world.setTile(Tile.random(new Tile[]{Tiles.floor1, Tiles.floor2}), mx, my);
                        } else {
                            System.out.println(world.getTile(mx, my).getHealth());
                            if (world.getTile(mx, my).getHealth() == world.getTile(mx, my).getMaxHealth() - 1) {
                                world.getTile(mx, my).addSprite(damage1.getSpriteList());
                            } else if (world.getTile(mx, my).getHealth() == world.getTile(mx, my).getMaxHealth() / 2) {
                                world.getTile(mx, my).addSprite(Tiles.damage2.getSpriteList());
                            } else if (world.getTile(mx, my).getHealth() == 1) {
                                world.getTile(mx, my).addSprite(Tiles.damage3.getSpriteList());
                            }
                        }
                    }
                }
            } else {
                player.getPosition().set(mx, my);
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        mainTheme.dispose();
        guiBatch.dispose();
        stage.dispose();
    }
}
