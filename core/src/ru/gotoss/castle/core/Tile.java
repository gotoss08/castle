package ru.gotoss.castle.core;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import ru.gotoss.castle.Settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by gotos on 7/5/2016.
 */
public class Tile {

    private static Random rng = new Random();
    private String name;
    private int health;
    private int maxHealth;
    private boolean solid;
    private boolean breakable;

    private List<Sprite> spriteList = new ArrayList<Sprite>();

    public Tile() {
    }

    public Tile(String name, Sprite sprite) {
        this.setName(name);
        this.addSprite(sprite);
        setSolid(false);
    }

    public Tile(String name, Sprite sprite, boolean solid) {
        this.setName(name);
        this.addSprite(sprite);
        this.setSolid(solid);
        this.setMaxHealth(1);
        this.setHealth(this.getMaxHealth());
    }

    public Tile(String name, Sprite sprite, int maxHealth, boolean solid) {
        this.setName(name);
        this.addSprite(sprite);
        this.setMaxHealth(maxHealth);
        this.setSolid(solid);
        this.setHealth(maxHealth);
    }

    public Tile(String name, Sprite sprite, int health, int maxHealth, boolean solid) {
        this.setName(name);
        this.addSprite(sprite);
        this.setHealth(health);
        this.setMaxHealth(maxHealth);
        this.setSolid(solid);
    }

    public Tile(String name, Sprite sprite, int health, int maxHealth, boolean solid, boolean breakable) {
        this.name = name;
        this.addSprite(sprite);
        this.health = health;
        this.maxHealth = maxHealth;
        this.solid = solid;
        this.breakable = breakable;
    }

    public Tile(String name, int health, int maxHealth, boolean solid, boolean breakable) {
        this.name = name;
        this.health = health;
        this.maxHealth = maxHealth;
        this.solid = solid;
        this.breakable = breakable;
    }

    public static Tile random(Tile[] tiles) {
        return tiles[rng.nextInt(tiles.length)];
    }

    public static <T> T random(T[] ts) {
        return ts[rng.nextInt(ts.length)];
    }

    public void draw(SpriteBatch batch, int x, int y) {
        for (int i = 0; i < getSpriteList().size(); i++) {
            batch.draw(getSpriteList().get(i), x * Settings.TILE_SIZE, y * Settings.TILE_SIZE, Settings.TILE_SIZE, Settings.TILE_SIZE);
        }
    }

    public Tile copy() {
        Tile tile = new Tile(getName(), getHealth(), getMaxHealth(), isSolid(), isBreakable());
        tile.setSpriteList(getSpriteList());
        return tile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addSprite(Sprite sprite) {
        getSpriteList().add(sprite);
    }

    public void addSprite(List<Sprite> sprites) {
        for (Sprite s : sprites) {
            getSpriteList().add(s);
        }
    }

    public List<Sprite> getSpriteList() {
        return spriteList;
    }

    public void setSpriteList(List<Sprite> spriteList) {
        this.spriteList = spriteList;
    }

    public Sprite getSprite(int id) {
        return getSpriteList().get(0);
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean solid) {
        this.solid = solid;
    }

    public boolean isBreakable() {
        return breakable;
    }

    public void setBreakable(boolean breakable) {
        this.breakable = breakable;
    }
}
