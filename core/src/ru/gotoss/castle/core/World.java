package ru.gotoss.castle.core;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import ru.gotoss.castle.assets.SpriteTextureAssets;
import ru.gotoss.castle.tiles.FloorTile;
import ru.gotoss.castle.tiles.GoldVineTile;
import ru.gotoss.castle.tiles.WallTile;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by gotos on 7/5/2016.
 */
public class World {

    // WORLD
    private Tile[][] tiles;

    private int width;
    private int height;
    private Random rng = new Random();
    // CREATURES
    private List<Creature> creatures;

    public World() {
        width = 100;
        height = 100;
    }

    //TODO create WorldBuilder class
    public World(Tile[][] tiles) {
        this.tiles = tiles;
        width = tiles.length;
        height = tiles[0].length;
    }

    public World(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Tile getTile(int x, int y) {
        if (x < 0 || x > getWidth() || y < 0 || y > getHeight()) {
            return null;
        } else {
            return tiles[x][y];
        }
    }

    public void setTile(Tile tile, int x, int y) {
        if (x < 0 || x > getWidth() || y < 0 || y > getHeight()) {
            return;
        } else {
            tiles[x][y] = tile;
        }
    }

    public Creature creature(int x, int y) {
        for (Creature c : creatures) {
            if ((int) c.getPosition().x == x && (int) c.getPosition().y == y) {
                return c;
            }
        }
        return null;
    }

    public void putCreature(Creature creature) {
        if (!creatures.contains(creature)) {
            creatures.add(creature);
        }
    }

    public void removeCreature(Creature creature) {
        if (creatures.contains(creature)) {
            creatures.remove(creature);
        }
    }

    public void render(SpriteBatch batch, int xOffset, int yOffset, int width, int height) {
        for (int i = xOffset; i < width; i++) {
            for (int j = yOffset; j < height; j++) {
                tiles[i][j].draw(batch, i, j);
            }
        }
        for (Creature creature : creatures) {
            creature.render(batch);
        }
    }


    public void generate() {
        System.out.println("generating world");
        tiles = new Tile[getWidth()][getHeight()];
        creatures = new ArrayList<Creature>();
        for (int i = 0; i < getWidth(); i++) {
            System.out.println(String.format("generating [%s]%%", i));
            for (int j = 0; j < getHeight(); j++) {
                switch (rng.nextInt(3)) {
                    case 0:
                        tiles[i][j] = new FloorTile(SpriteTextureAssets.floor1Texture);
                        break;
                    case 1:
                        tiles[i][j] = new FloorTile(SpriteTextureAssets.floor2Texture);
                        break;
                    case 2:
                        if (rng.nextInt(100) > 80)
                            tiles[i][j] = new GoldVineTile(SpriteTextureAssets.goldVineTexture);
                        else
                            tiles[i][j] = new WallTile(SpriteTextureAssets.wallTexture);
                        break;
                }
            }
        }
    }

    public void putCreatureAtRandomPlace(Creature creature) {
        int x;
        int y;

        System.out.println("placing creature at random location....");
        do {
            x = (int) (Math.random() * getWidth());
            y = (int) (Math.random() * getHeight());
            System.out.println(String.format("random x=%s y=%s solid(%s)", x, y, getTile(x, y).isSolid()));
        }
        while (getTile(x, y).isSolid());
        System.out.println(String.format("final x=%s y=%s", x, y));

        creature.setPosition(new Vector2(x, y));
        putCreature(creature);
    }

    public boolean checkBorder(int x, int y) {
        if (x < getWidth() && x > 0 && y < getHeight() && y > 0)
            return true;
        else
            return false;
    }

    public void dispose() {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[0].length; j++) {
                for (Sprite s : tiles[i][j].getSpriteList()) {
                    s.getTexture().dispose();
                }
            }
        }

        for (Creature c : creatures) {
            c.dispose();
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
