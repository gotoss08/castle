package ru.gotoss.castle.core;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import ru.gotoss.castle.Settings;

/**
 * Created by gotos on 7/5/2016.
 */
public class Creature {

    private String name;

    private Sprite sprite;

    private Vector2 position;

    private int speed;

    private int damage;

    public Creature(String name, Sprite sprite, Vector2 position) {
        this.setName(name);
        this.setSprite(sprite);
        this.setPosition(position);
    }

    public void render(SpriteBatch batch) {
        batch.draw(getSprite(), getPosition().x * Settings.TILE_SIZE, getPosition().y * Settings.TILE_SIZE, Settings.TILE_SIZE, Settings.TILE_SIZE);
    }


    public void dispose() {
        getSprite().getTexture().dispose();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}