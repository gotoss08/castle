package ru.gotoss.castle.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.gotoss.castle.GameApplication;
import ru.gotoss.castle.Settings;

public class DesktopLauncher {

    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.resizable = false;
        config.title = Settings.TITLE;
        config.width = Settings.WIDTH;
        config.height = Settings.HEIGHT;
        new LwjglApplication(new GameApplication(), config);
    }
}
